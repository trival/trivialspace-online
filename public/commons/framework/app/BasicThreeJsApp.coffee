### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "lib/utils/js/functions"
  "lib/utils/threejs/helpers"
  "lib/utils/sound/functions"
  "external/canvas-to-blob.min.js"
  "js!external/pxloader/PxLoader!order"
  "js!external/pxloader/PxLoaderImage!order"

], ({extend}
    {texture}
    {withAudioContext, loadSounds}) ->


  webglEnabled = (-> 
    try 
      return !! window.WebGLRenderingContext && !! document.createElement('canvas').getContext 'experimental-webgl' 
    catch e  
      return false)()

  _deviceText = if window.WebGLRenderingContext then "graphics card" else "browser" 

  noWebGLMessage = "<p>Your #{_deviceText} does not seem to support <a href='http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation'>WebGL</a>.<br />
        Find out how to get it <a href='http://get.webgl.org/'>here</a>.<br />
        You will only be able to see a screenshot.</p>"

  errorMessage = "<p>Something went wrong. Please come back later.<br />We are trying to fix the problem.</p>"
  loadingAssetsMessage = "<p>Downloading application assets...</p>"
  initializingMessage = "<p>Initializing the application...</p>"


  class BasicThreeJsApp

    # options
    domElement: null
    statusElement: null

    errorMessage: errorMessage
    initializingMessage: initializingMessage
    loadingAssetsMessage: loadingAssetsMessage
    screenshotUrl: ""
    screenshotCallback: (dataUrl) -> window.open dataUrl

    directRender: true
    fullscreen: false
    trackMouse: false

    # state
    width: 800
    height: 600
    mouseX: 0
    mouseY: 0
    paused: false
    webglEnabled: webglEnabled

    # internal objects
    scene: null
    renderer: null
    camera: null
    modelLoader: null
    textureUrls: []
    textures: []
    soundUrls: []
    sounds: []
    soundContext: null
    soundDestination: null

    constructor: (configObject) ->

      extend @, configObject
      THREE.EventDispatcher.call this

      if typeof @domElement is 'string'
        @domElement = document.getElementById @domElement
      else unless @domElement
        @domElement = document.createElement "div"
        document.body.appendChild @domElement

      if @fullscreen
        @height = window.innerHeight
        @width = window.innerWidth
      else
        @domElement.style.width = @width + 'px'
        @domElement.style.height = @height + 'px'

      @modelLoader = new THREE.JSONLoader()
      load = (model, texture_path) ->
        jsonModel = JSON.parse model
        (@parse jsonModel, texture_path).geometry 

      @modelLoader.load = load


    init: ->


    update: ->


    start: ->

      handleError = (e) =>
        console.error e.message
        @dispatchEvent type: "error", error: e
        @statusElement?.innerHTML = @errorMessage

      try 
        if webglEnabled
          textureLoader = new PxLoader
          textures = (textureLoader.addImage url for url in @textureUrls)

          init = =>
            @statusElement?.innerHTML = @initializingMessage
            setTimeout (=>
              try
                @textures = (texture image for image in textures)
                @init() 
                @_animate()
                setTimeout (=>
                  @renderer.domElement.className = ""
                  @dispatchEvent type: "draftLoaded"
                ), 100
              catch e
                handleError e
            ), 10
          
          loadSound = 
            if @soundUrls.length > 0
              => 
                withAudioContext ((ctx) =>
                  @soundContext = ctx
                  loadSounds ctx, @soundUrls, ((soundList) =>
                      @sounds = soundList
                      @soundDestination = ctx.createGain()
                      @soundDestination.gain.value = 1;
                      @soundDestination.connect ctx.destination
                      init()
                    ), init
                ), init

            else 
              init

          loadTextures = 
            if textures?.length > 0
              => 
                @statusElement?.innerHTML = @loadingAssetsMessage
                textureLoader.addCompletionListener loadSound
                textureLoader.start()
            else         
              loadSound

          @_internalInit()
          loadTextures()

        else
          @statusElement?.innerHTML = noWebGLMessage
          if @screenshotUrl
            @domElement.style.background = 
              "url(\"#{@screenshotUrl}\") no-repeat fixed center center / cover"

      catch e
        handleError e


    togglePause: ->
      @paused = not @paused
      @_animate()  unless @paused
      if @soundDestination
        @soundDestination.gain.value =
          if @paused then 0 else 1
      @dispatchEvent 
        type: "pauseStateChanged"
        paused: @paused


    screenshot: (callback) ->
      @screenshotCallback = callback if callback
      @_screenshot = true
      @_render() if @paused


    _animate: =>
      unless @paused
        requestAnimationFrame @_animate
        @_render()


    _internalInit: ->
      @scene ?= new THREE.Scene()
      @camera ?= new THREE.PerspectiveCamera 60, @width / @height, 0.01, 100
      @scene.add @camera
      @renderer ?= new THREE.WebGLRenderer antialias: true
      @renderer.setSize @width, @height
      @renderer.domElement.className = 'loading'
      @domElement.appendChild @renderer.domElement

      if @trackMouse
        @domElement.addEventListener 'mousemove', (evt) =>
          rect = @renderer.domElement.getBoundingClientRect()
          x = evt.clientX - rect.left
          y = evt.clientY - rect.top
          @mouseX = (x / @width) * 2 - 1
          @mouseY = (y / @height) * 2 - 1

      document.addEventListener "keypress", (evt) =>
        key = evt.charCode
        @togglePause() if "p".charCodeAt(0) is key
        @screenshot() if "P".charCodeAt(0) is key

      if @fullscreen
        window.addEventListener "resize", @onResize
      else
        @domElement.addEventListener "resize", @onResize


    _render: ->
      @update()
      @renderer.render @scene, @camera  if @directRender
      
      if @_screenshot and @renderer?.domElement?.toBlob?
        @renderer.domElement.toBlob (blob) =>
          @screenshotCallback URL.createObjectURL blob
        @_screenshot = false  


    onResize: (event) =>
      console.debug "resize event"
      @width = if @fullscreen then window.innerWidth else @domElement.innerWidth
      @height = if @fullscreen then window.innerHeight else @domElement.innerHeight
      console.debug "width: " + @width
      console.debug "height: " + @height
      @renderer.setSize @width, @height
      @camera.aspect = @width / @height
      @camera.updateProjectionMatrix()
      @_render() if @paused
