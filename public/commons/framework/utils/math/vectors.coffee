### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define ->

  add =  (vec1, vec2) ->
    for i in [0..vec1.length-1]
      vec1[i] + vec2[i]


  addScalar = (vec, scalar) ->
    for i in vec
      i + scalar


  sub = (vec1, vec2) ->
    for i in [0..vec1.length-1]
      vec1[i] - vec2[i]


  subScalar = (vec, scalar) ->
    for i in vec
      i - scalar


  mul = (vec, scalar) ->
    for val in vec
      val * scalar


  div = (vec, scalar) ->
    if scalar
      val / scalar for val in vec
    else undefined


  length = (vec) ->
    sum = 0
    sum += val * val for val in vec
    Math.sqrt sum


  normalize = (vec) ->
    div vec, length vec


  limit = (vec, maxLenght) ->
    if maxLenght < length vec
      mul normalize(vec), maxLenght
    else
      vec

  isEqual = (vec1, vec2) ->
    equal = true
    for i in [0..vec1.length-1]
      unless vec1[i] == vec2[i]
        equal = false
    equal


  exports =
    add: add
    addScalar: addScalar
    sub: sub
    subScalar: subScalar
    mul: mul
    div: div
    length: length
    normalize: normalize
    limit: limit
