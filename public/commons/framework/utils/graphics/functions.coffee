### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [
  "../math/functions"
], ({interpolate}) ->

  mixColors: ([r1, g1, b1, a1], [r2, g2, b2, a2]) ->
    step = a2 / 255.0
    r = Math.floor interpolate r1, r2, step
    g = Math.floor interpolate g1, g2, step
    b = Math.floor interpolate b1, b2, step
    [r, g, b, a1]


  grey: (scale, alpha) ->
    alpha ?= 255
    [scale, scale, scale, alpha]


  colorToCSS: ([r, g, b, a]) ->
    "rgba(#{r}, #{g}, #{b}, #{a / 255})"


  newCanvas: (width, height, init) ->
    canvas = document.createElement "canvas"
    canvas.width = width
    canvas.height = height
    ctx = canvas.getContext "2d"
    init ctx, width, height
    [canvas, ctx]

