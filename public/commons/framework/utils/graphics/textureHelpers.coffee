### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

	"lib/utils/math/noise"
  "lib/utils/graphics/imageDataContext"
  "lib/utils/graphics/functions"

], ({tileNoise},
    {getPixelContext},
    {grey}) ->


  exports = 

    createTileNoiseTexture: (width, height, startX, startY) ->
      canvas = document.createElement "canvas"
      canvas.width = width
      canvas.height = height
      ctx = canvas.getContext "2d"
      pixels = getPixelContext ctx.getImageData 0, 0, width, height

      noise1 = tileNoise width, height, startX, startY
      noise2 = tileNoise width, height, startX * 2.1, startY * 2.1
      noise3 = tileNoise width, height, startX * 4.2, startY * 4.2 
      noise4 = tileNoise width, height, startX * 8.3, startY * 8.3

      for i in [0..noise1.length-1]
        x = i % width
        y = Math.floor i / height
        to8Bit = (float) -> Math.floor (float + 1) * 127
        color = [to8Bit(noise1[i]), to8Bit(noise2[i]), to8Bit(noise3[i]), to8Bit(noise4[i])] 
        pixels.setColorAt [x, y], color

      ctx.putImageData pixels.imageData, 0, 0
      canvas