### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define ->

  BLACK: [0, 0, 0, 255]
  WHITE: [255, 255, 255, 255]
