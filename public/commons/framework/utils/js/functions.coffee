### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define ->
  
  partial: (fn, argMem...) ->
    ->
      arg = i = 0
      args = Array::slice.call argsMem
      while arg < arguments.length
        if args[i] is undefined
          args[i] = arguments[arg++]
        i++
      fn.apply this, args


  extend: (obj, mixin) ->
    obj[key] = value for key, value of mixin
    obj
