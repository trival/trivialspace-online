uniform vec3 ambientLightColor;

#if MAX_DIR_LIGHTS > 0

    uniform vec3 directionalLightColor[ MAX_DIR_LIGHTS ];
    uniform vec3 directionalLightDirection[ MAX_DIR_LIGHTS ];

#endif

#if MAX_POINT_LIGHTS > 0

    uniform vec3 pointLightColor[ MAX_POINT_LIGHTS ];
    uniform vec3 pointLightPosition[ MAX_POINT_LIGHTS ];
    uniform float pointLightDistance[ MAX_POINT_LIGHTS ];
    varying vec4 vPointLight[ MAX_POINT_LIGHTS ];

#endif

vec3 calculateVertexDirectionalLighting(in vec3 inNormal){
    vec3 outColor = vec3(0.0);
    #if MAX_DIR_LIGHTS > 0
        for( int i = 0; i < MAX_DIR_LIGHTS; i++ ) {
            vec4 lDirection = viewMatrix * vec4( directionalLightDirection[ i ], 0.0 );
            float directionalLightWeighting = max( dot( inNormal, normalize( lDirection.xyz ) ), 0.0 );
            outColor += directionalLightColor[ i ] * directionalLightWeighting;
        }
    #endif
    return outColor;
}

void calculatePointLightVectors(vec3 mvPosition){
    #if MAX_POINT_LIGHTS > 0
        for( int i = 0; i < MAX_POINT_LIGHTS; i++ ) {
            vec4 lPosition = viewMatrix * vec4( pointLightPosition[ i ], 1.0 );
            vec3 lVector = lPosition.xyz - mvPosition;
            float lDistance = 1.0;
            if ( pointLightDistance[ i ] > 0.0 )
                lDistance = 1.0 - min( ( length( lVector ) / pointLightDistance[ i ] ), 1.0 );
            lVector = normalize( lVector );
            vPointLight[ i ] = vec4( lVector, lDistance );
        }
    #endif
}

vec3 calculateVertexPointLighting(in vec4 pointLight[MAX_POINT_LIGHTS], in vec3 inNormal){
    vec3 outColor = vec3(0.0);
    #if MAX_POINT_LIGHTS > 0
        for( int i = 0; i < MAX_POINT_LIGHTS; i++ ) {
            float pointLightWeighting = max( dot( inNormal, pointLight[i].xyz ), 0.0 );
            outColor += pointLightColor[ i ] * pointLightWeighting * pointLight[i].w;
        }
    #endif
    return outColor;
}