varying vec3 vNormal;
varying vec3 vViewPos;

void main(void)
{
    vec4 mvPosition = modelViewMatrix * vec4(position,1.0);
    gl_Position = projectionMatrix * mvPosition;
    calculatePointLightVectors(mvPosition.xyz, vPointLight);
    vViewPos = -normalize(mvPosition.xyz);
    vNormal = normalize(normalMatrix * normal);
}