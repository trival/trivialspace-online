#if MAX_POINT_LIGHTS > 0
    uniform vec3 pointLightPosition[ MAX_POINT_LIGHTS ];
    uniform float pointLightDistance[ MAX_POINT_LIGHTS ];
    varying vec4 vPointLight[ MAX_POINT_LIGHTS ];
#endif

#if MAX_DIR_LIGHTS > 0
    uniform vec3 directionalLightDirection[ MAX_DIR_LIGHTS ];
    varying vec3 vDirLight[ MAX_DIR_LIGHTS ];
#endif

varying vec3 vNormal;

void main(void)
{
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
    vNormal = normalize(normalMatrix * normal);
    
    vec3 mvPos = (modelViewMatrix * vec4(position,1.0)).xyz;
    
    #if MAX_POINT_LIGHTS > 0
	for( int i = 0; i < MAX_POINT_LIGHTS; i++ ) {
            vec4 lvPos = viewMatrix * vec4( pointLightPosition[ i ], 1.0 );
            vec3 lVector = lvPos.xyz - mvPos.xyz;
	
            float lDistance = 1.0;
            if ( pointLightDistance[ i ] > 0.0 )
                lDistance = 1.0 - min( ( length( lVector ) / pointLightDistance[ i ] ), 1.0 );

            vPointLight[ i ] = vec4(lVector,lDistance);
	}
    #endif
	
    #if MAX_DIR_LIGHTS > 0
	for( int i = 0; i < MAX_DIR_LIGHTS; i++ ) {
            vec4 lvDir = viewMatrix * vec4( directionalLightDirection[i], 1.0);
            //vDirLight[i] = (normalize(lvDir.xyz)) * tbnMat;
            vDirLight[i] = normalize(lvDir.xyz);
	}
    #endif
}