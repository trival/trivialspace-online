###
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "jquery"
  "underscore"
  "ui/scrollwidget/scroll-widget"
  "./jquery.nanoscroller.min.js"

], ($,
    {partial}) ->

  $.noConflict()


  MENU_HIDE_TIMEOUT = 1800


  showingInfo = false
  showingMenu = true

  $header = null
  $main = null
  $menu = null
  $text = null
  $navigation = null
  $scrollable = null

  $menuButton = null
  $infoButton = null
  $pauseButton = null
  $fullscreenButton = null
  $screenshotButton = null

  mainElements = []

  hideTimer = 0


  initResponsiveElements = ->
    $header = $ 'header'
    $main = $ '#information-frame'
    $menu = $ '#draft-menu'
    $text = $ '#information-content'
    $navigation = $ '#draft-navigation-info'
    $scrollable = $ '#draft-menu, #information-content'
    $menuButton = $ '#menu-button'
    $infoButton = $ '#info-button'
    $pauseButton = $ '#pause-button'
    $fullscreenButton = $ '#fullscreen-button'
    $screenshotButton = $ '#screenshot-button'

    mainElements = [ $header, $main, $menuButton ]


  showMenu = ->
    $header.css 'display': "block"
    setTimeout (->
      element.removeClass("disabled") for element in mainElements
      showingMenu = true
    ),10


  hideMenu = ->
    element.addClass("disabled") for element in mainElements
    $navigation.removeClass 'showing'
    showingMenu = false


  hideMenuTimeout = ->
    if showingMenu
      clearTimeout hideTimer
      hideTimer = setTimeout hideMenu, MENU_HIDE_TIMEOUT


  showInfoContent = ->
    element.addClass 'show-info' for element in mainElements
    $navigation.removeClass 'showing'
    showingInfo = true


  hideInfoContent = ->
    element.removeClass 'show-info' for element in mainElements
    $infoButton.removeClass 'selected'
    showingInfo = false


  arrangeContentHeight = ->
    winHeight = $(window).height()
    headerHeight = $header.outerHeight()
    footerHeight = $('#main-footer').outerHeight()
    contentHeight = winHeight - (headerHeight + footerHeight)

    $main.css 'height', winHeight - headerHeight
    $scrollable.css 'height', contentHeight

    $menu.trigger 'scrollstatechange'
    $text.nanoScroller()
    setTimeout (-> $menu.trigger 'scrollstatechange'), 300


  scrollTo = (queryString) ->
    scrollLength = 0
    $article = $text.find '.content'
    unless queryString is '#scrollpane-scroll-to-top'
      $scrollTo = $(queryString)
      scrollLength = $scrollTo.offset().top - $article.offset().top + $article.scrollTop() - 40

    $article.animate scrollTop: scrollLength


  # ------------------------- initialize layout -----------------------------

  # init all important clickevents
  initButtonActions = ->
    $menuButton.click ->
      if showingInfo
        hideInfoContent()
      else if showingMenu
        hideMenu()
      else
        showMenu()

    $infoButton.click ->
      if $infoButton.hasClass 'selected'
        hideInfoContent()
      else
        showInfoContent()
        $infoButton.addClass 'selected'

    $fullscreenButton.click ->
      elem = document.getElementById "draft-frame"
      if elem.requestFullscreen
        elem.requestFullscreen()
      else if elem.mozRequestFullScreen
        elem.mozRequestFullScreen()
      else if (elem.webkitRequestFullscreen)
        elem.webkitRequestFullscreen()


  #init info text scroll behavior
  initInfoText = ->
    $text.on 'click', 'a', ->
      if $(this).attr('href')[0] == '#'
        scrollTo $(this).attr 'href'
        false


  # Basic interaction events
  initEvents = ->

    $header.on "transitionend webkitTransitionEnd", (evt) ->
      if evt.target is this and not showingMenu
        $header.css 'display', 'none'
        hideInfoContent() if showingInfo

    element.on 'mouseenter', showMenu for element in mainElements

    element.on 'mouseenter mousemove', (-> clearTimeout hideTimer) for element in mainElements


  # Bind events on the running draft
  bindEventsOnDraft = (draft) ->
    if draft.webglEnabled

      $pauseButton.click ->
        draft.togglePause()

      $screenshotButton.click ->
        draft.screenshot()

      draft.addEventListener 'pauseStateChanged', (evt) ->
        if evt.paused
          $pauseButton.find('i').removeClass('icon-pause').addClass 'icon-play'
          $pauseButton.next().html 'resume animation'
        else
          $pauseButton.find('i').removeClass('icon-play').addClass 'icon-pause'
          $pauseButton.next().html 'pause animation'

      draft.addEventListener 'draftLoaded', ->
        hideMenuTimeout()
        $('#status-message').hide()
        $navigation.show()
        setTimeout (-> element.off 'mousemove' for element in mainElements), MENU_HIDE_TIMEOUT
        $('#draft-frame').on 'mouseenter', hideMenuTimeout

    else
      button.hide() for button in [$fullscreenButton, $pauseButton, $screenshotButton]


  addHeaderToTopLinks = ->
    $('#information-content h2').append(
      '&nbsp;&nbsp;<a href="#scrollpane-scroll-to-top"><i class="icon-angle-up icon-small"></i></a>\n')


  # The initialization of static content
  initStatic = ->
    initResponsiveElements()
    initInfoText()

    # set initial layout
    arrangeContentHeight()
    setTimeout arrangeContentHeight, 5
    $(window).on 'resize', arrangeContentHeight
    $menu.scrollPane()
    $text.nanoScroller()


  #initialization of all content
  init = (ready) ->
    initStatic()
    initButtonActions()
    initEvents()

    # call the ready callback if it exists and init Events on running draft
    ready?()


  # --------------------------- public interface ---------------------------

  exports =
    init: init
    initStatic: initStatic
    bindEventsOnDraft: bindEventsOnDraft
    hideMenuTimeout: hideMenuTimeout
    addHeaderToTopLinks: addHeaderToTopLinks
