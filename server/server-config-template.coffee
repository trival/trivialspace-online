exports.PUBLIC_PATH = __dirname + "/../public"

db_user = ""
db_password = ""
db_domain = "127.0.0.1"
db_port = 5984
db_database = "trivialspace"

exports.DB_DOMAIN = db_domain
exports.DB_PORT = db_port
exports.DB_USER = db_user
exports.DB_PASSWORD = db_password
exports.DB_DATABASE = db_database

db_url = "http://"
db_url += db_user if db_user
db_url += ":" + db_password if db_password.length > 0
db_url += "@" if db_user
db_url += db_domain
db_url += ":" + db_port if db_port > 0

exports.DB_URL = db_url

exports.SERVER_PORT = process.env.PORT || 8000
