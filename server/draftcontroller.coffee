nano = require 'nano'
config = require './server-config'
async = require 'async'
{partial, extend} = require 'lodash'


dbString = config.DB_URL + '/' + config.DB_DATABASE
db = nano dbString

title = "TRIVIAL SPACE - "


# ----------------- route initialization -------------------

exports.init = (app) ->

  app.get '/about', (req, res, next) ->
    async.series 
      drafts: getMenuDrafts
      content: partial res.render.bind(res), 'about.html'
      catchErr next, (params) ->
        res.render 'index-static', extend params,
          header: "ABOUT TRIVIAL SPACE"
          title: title + "ABOUT"


  app.param 'draftId', (req, res, next, draftId) ->
    db.get draftId, catchErr next, (doc) ->
      if not doc.active
        next "draft not found"
      else
        req.params.draft = doc
        next()


  app.get "/", (req, res, next) ->
    getMenuDrafts catchErr next, (drafts) ->
      res.render 'index-draft',
        title: title + drafts[0].name.toUpperCase()
        draft: drafts[0]
        drafts: drafts


  app.get "/:draftId", (req, res, next) ->
    draft = req.params.draft
    getMenuDrafts catchErr next, (drafts) ->
      res.render 'index-draft',
        title: title + draft.name.toUpperCase()
        draft: draft
        drafts: drafts
        
    
# -------------------- helper methods ---------------------

getMenuDrafts = (callback) ->
  db.view 'views','draftsByLastChange', descending: true, (err, results) ->
    if err
      callback err
    else if not results or results.length is 0
      callback "no drafts found"
    else
      drafts = (result.value for result in results.rows when result.value.active)
      callback null, drafts


catchErr = (next, successCallback) ->
  return (err, result) ->
    if err
      next err
    else 
      successCallback result 

